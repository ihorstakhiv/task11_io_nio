package com.epam.models;

import java.io.Serializable;

public class Human implements Serializable {
    private String name;

    public Human(String name) {

        this.name = name;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                '}';
    }
}
