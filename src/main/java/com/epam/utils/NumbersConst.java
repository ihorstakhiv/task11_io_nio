package com.epam.utils;

public class NumbersConst {

  private static final int START_CYCLE = 0;
  private static int LAST_FILE_ELEMENT = -1;
  private static int Mb_COUNT = 10;
  private static int Mb = 1024;
  private static int FLAG_READER = 0;
  private static int FLAG_WRITER = 1;
  private static int FIRST_CHOOSE = 1;
  private static int SECOND_CHOOSE = 2;


  public static int getMb() {
    return Mb;
  }

  public static int getStartCycle() {
    return START_CYCLE;
  }

  public static int getLastFileElement() {
    return LAST_FILE_ELEMENT;
  }

  public static int getMb_COUNT() {
    return Mb_COUNT;
  }

  public static int getFlagReader() {
    return FLAG_READER;
  }

  public static int getFlagWriter() {
    return FLAG_WRITER;
  }

  public static int getFirstChoose() {
    return FIRST_CHOOSE;
  }

  public static int getSecondChoose() {
    return SECOND_CHOOSE;
  }
}
