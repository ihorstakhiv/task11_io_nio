package com.epam.utils;

public class ConstTxt {

    final static String CHOOSE_LANGUAGE = "Please chose a language \n" + "\n1) English" + "\n2) Українська \n";
    final static String INVALID_VARIABLE = "\nInvalid variable, please enter from 1 to 2\n";

    public static String getChooseLanguage() {
        return CHOOSE_LANGUAGE;
    }

    public static String getInvalidVariable() {
        return INVALID_VARIABLE;
    }
}
