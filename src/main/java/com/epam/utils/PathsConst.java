package com.epam.utils;

public class PathsConst {

    final static String TEXT_EXAMPLE_PATH = "D:\\JAVA\\task11_IO_NIO\\src\\main\\resources\\textExample.txt";
    final static String SOME_BUFFER_JAVA_PATH = "D:\\JAVA\\task11_IO_NIO\\src\\main\\java\\com\\epam\\models\\SomeBuffer.java";
    final static String COMMENT = "//";
    final static String MY_TEST_PACKAGE_PATH = "D:\\Робочий стіл\\testPackage";
    final static String TEST_SERIALIZE_PATH = "D:\\JAVA\\task11_IO_NIO\\src\\main\\resources\\testSerializeFile";

    public static String getMyTestPackagePath() {
        return MY_TEST_PACKAGE_PATH;
    }

    public static String getTextExamplePath() {
        return TEXT_EXAMPLE_PATH;
    }

    public static String getSomeBufferJavaPath() {
        return SOME_BUFFER_JAVA_PATH;
    }

    public static String getCOMMENT() {
        return COMMENT;
    }

    public static String getTestSerializePath() {
        return TEST_SERIALIZE_PATH;
    }
}
