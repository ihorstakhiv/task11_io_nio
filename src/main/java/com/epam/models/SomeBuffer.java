package com.epam.models;

import com.epam.utils.NumbersConst;
import com.epam.utils.PathsConst;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class SomeBuffer {

    //Comment for task3
    private int flag;
    private ArrayList<String> buffer;

    public SomeBuffer() {
        buffer = new ArrayList<>();
        flag = NumbersConst.getFlagReader();
    }

    private void reader() {
        flag = NumbersConst.getFlagReader();
        Path path = Paths.get(PathsConst.getMyTestPackagePath());
        try (BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            String currentLine = reader.readLine();
            while (!Objects.isNull(currentLine)) {
                buffer.add(currentLine);
                currentLine = reader.readLine();
            }
        } catch (IOException e) {
            e.getStackTrace();
        }
    }
    //Comment for task3

    private void writer(String... arg) {
        flag = NumbersConst.getFlagWriter();
        buffer.addAll(Arrays.asList(arg));
    }

    private void flip() {
        if (flag == NumbersConst.getFlagReader()) {
            writer("text example");
        } else {
            reader();
        }
    }
}