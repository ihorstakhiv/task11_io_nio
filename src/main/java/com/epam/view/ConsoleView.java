package com.epam.view;

import com.epam.controllers.Controller;
import com.epam.utils.ConstTxt;
import com.epam.utils.NumbersConst;
import com.epam.utils.PathsConst;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.*;

public class ConsoleView implements View {

    private Locale locale;
    private ResourceBundle resourceBundle;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        input = new Scanner(System.in);
        controller = new Controller();
        logger.info(ConstTxt.getChooseLanguage());
        try {
            int choose = input.nextInt();
            if (choose == NumbersConst.getFirstChoose())
                englishMenu();
            if (choose == NumbersConst.getSecondChoose())
                ukraineMenu();
            if (choose > NumbersConst.getSecondChoose())
                throw new NullPointerException();
        } catch (Exception exception) {
            logger.info(ConstTxt.getInvalidVariable());
            new ConsoleView();
        }
    }

    private void putResourceBundle() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
    }

    private void putMethods() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printSerializeAndDeserializeTest);
        methodsMenu.put("2", this::printCompareBufferAndUsual);
        methodsMenu.put("3", this::printTestJavaFileOnCommentary);
        methodsMenu.put("4", this::printContentsDirectory);
        methodsMenu.put("5", this::languageMenu);
    }

    private void printCompareBufferAndUsual() throws IOException {
        logger.info("\nTime usual reader : " + controller.testTimeReaderUsual());
        logger.info("\nTime buffered reader : " + controller.testTimeReaderBuffered());
        logger.info("\nDifferent in second : " + controller.compareBufferAndUsual());
    }

    private void printSerializeAndDeserializeTest() throws Exception {
        logger.info("\nSerialization...\nPlease set human name :");
        controller.serializationTest(input.nextLine());
        logger.info("\nDeserialize...");
        logger.info("\n" + controller.deserializationTest());
    }

    private void printContentsDirectory() throws IOException {
        File file = new File(PathsConst.getMyTestPackagePath());
        if (file.exists()) {
            printDirectory(file, "");
        } else {
            logger.error("directory do not exists");
        }
    }

    private void printDirectory(File file, String str) {
        logger.info("\n"+str + "Directory: " + file.getName());
        str = str + "  ";
        File[] fileNames = file.listFiles();
        assert fileNames != null;
        for (File f : fileNames) {
            if (f.isDirectory()) {
                printDirectory(f, str);
            } else {
                logger.info("\n"+str + "File: " + f.getName());
            }
        }
    }

    private void printTestJavaFileOnCommentary() throws IOException {
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(PathsConst.getSomeBufferJavaPath())));
        while (true) {
            String line = in.readLine();
            if (line == null) {
                break;
            }
            int index = line.indexOf("//");
            if (index != NumbersConst.getLastFileElement()) {
                logger.info("\n"+line.substring(index));
            }
        }
        in.close();
    }

    private void englishMenu() {
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void ukraineMenu() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("MyMenu", locale);
        putResourceBundle();
        putMethods();
        show();
    }

    private void languageMenu() {
        new ConsoleView();
    }

    private void outputMenu() {
        logger.info("\n*********************************************       MENU       *********************************************   \n");
        for (String str : menu.values()) {
            logger.info("\n" + str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("\nPlease, select menu point:\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}