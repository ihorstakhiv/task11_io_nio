package com.epam.controllers;

import com.epam.models.Human;
import com.epam.utils.NumbersConst;
import com.epam.utils.PathsConst;
import java.io.*;

public class Controller {

    public int testTimeReaderUsual() throws IOException {
        long startTime = System.nanoTime();
        InputStream inputstream = new FileInputStream(PathsConst.getTextExamplePath());
        int data = inputstream.read();
        while (data != NumbersConst.getLastFileElement()) {
            data = inputstream.read();
        }
        inputstream.close();
        return (int) (System.nanoTime() - startTime);
    }

    public int testTimeReaderBuffered() throws IOException {
        long startTime = System.nanoTime();
        int bufferSize = NumbersConst.getMb_COUNT() * NumbersConst.getMb() * NumbersConst.getMb();
        DataInputStream in2 = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(PathsConst.getTextExamplePath()), bufferSize));
        try {
            while (true) {
                in2.readByte();
            }
        } catch (EOFException e) {
            e.getStackTrace();
        }
        in2.close();
        return (int) (System.nanoTime() - startTime);
    }

    public int compareBufferAndUsual() throws IOException {
        return testTimeReaderUsual() - testTimeReaderBuffered();
    }

    public void serializationTest(String name) throws IOException {
        FileOutputStream outputStream;
        ObjectOutputStream objectOutputStream = null;
        Human human = new Human(name);
        try {
            outputStream = new FileOutputStream(PathsConst.getTestSerializePath());
            objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(human);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert objectOutputStream != null;
        objectOutputStream.close();
    }

    public Object deserializationTest() throws IOException {
        FileInputStream inputStream;
        ObjectInputStream objectInputStream = null;
        Human human = null;
        try {
            inputStream = new FileInputStream(PathsConst.getTestSerializePath());
            objectInputStream = new ObjectInputStream(inputStream);
            human = (Human) objectInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert objectInputStream != null;
        objectInputStream.close();
        return human;
    }
}
